const fs = require('fs');

// reading lipsum.txt file

function readFile(callbackFunction) {

    const filePath = `${__dirname}/lipsum.txt`;

    fs.readFile(filePath, "utf-8", (err, data) => {
        if (err) {
            callbackFunction(err, data);
        } else {
            callbackFunction(err, data);
        }
    });
}

// adding all new created file name in filenames.txt

function appendFilename(fileName, callbackFunction) {

    const filenamesPath = `${__dirname}/filenames.txt`;

    fs.appendFile(filenamesPath, `${fileName}\n`, (err) => {
        if (err) {
            callbackFunction(err);
        } else {
            console.log(`${fileName} written in filename.txt`);
            callbackFunction(err);
        }
    });
}

// converting all the contents into uppercase and storing it into new txt file

function upperCaseFile(fileName, fileContent, callbackFunction) {

    const filePath = `${__dirname}/${fileName}`;
    const fileContentUpperCase = fileContent.toUpperCase();

    fs.writeFile(filePath, fileContentUpperCase, (err) => {
        if (err) {
            callbackFunction(err, fileContentUpperCase);
        } else {
            console.log(`${fileName} created`);
            appendFilename(fileName, (err) => {
                if (err) {
                    callbackFunction(err, fileContentUpperCase);
                } else {
                    callbackFunction(err, fileContentUpperCase);
                }
            });
        }
    });
}

// converting all the contents into lowercase, splitting it into new line and storing it into new txt file

function lowerCaseFile(fileName, fileContent, callbackFunction) {

    const filePath = `${__dirname}/${fileName}`;
    const fileContentLowerCase = fileContent.toLowerCase();

    const splitLowerCaseFile = fileContentLowerCase
        .split(". ")
        .join(".\n");

    fs.writeFile(filePath, splitLowerCaseFile, (err) => {
        if (err) {
            callbackFunction(err);
        } else {
            console.log(`${fileName} created`);
            appendFilename(fileName, (err) => {
                if (err) {
                    callbackFunction(err);
                } else {
                    callbackFunction(err);
                }
            });
        }
    });
}

// sorting all the contents from the new files and storing it into new txt file

function sortFilesContent(fileName, readFileName1, readFileName2, callbackFunction) {

    const filePath = `${__dirname}/`;

    fs.readFile(filePath + readFileName1, "utf-8", (err1, fileData1) => {

        if (err1) {
            callbackFunction(err1);
        } else {

            fs.readFile(filePath + readFileName2, "utf-8", (err2, fileData2) => {

                if (err2) {
                    callbackFunction(err2);
                } else {

                    let allFileData = fileData1 + fileData2;
                    allFileData = allFileData
                        .replaceAll('\n', ' ')
                        .split(' ')
                        .sort((word1, word2) => {
                            return word1.localeCompare(word2);
                        });

                    fs.writeFile(filePath + fileName, JSON.stringify(allFileData), (err3) => {
                        if (err3) {
                            callbackFunction(err3);
                        } else {
                            console.log(`${fileName} created`);
                            appendFilename(fileName, (err) => {
                                if (err) {
                                    callbackFunction(err);
                                } else {
                                    callbackFunction(err);
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

// deleting all the files according to the filename.txt

function deleteFiles(callbackFunction) {

    filePath = `${__dirname}/filenames.txt`;

    fs.readFile(filePath, "utf-8", (err, data) => {
        if (err) {
            callbackFunction(err);
        } else {

            const allFileName = data.split('\n');
            let index = 0;
            let countSuccess = 0;
            let countFail = 0;

            function recursiveDeleteFile(index, allFileName) {

                if (index === allFileName.length) {
                    return;
                } else if (allFileName[index] !== '') {
                    const filePath = `${__dirname}/${allFileName[index]}`;

                    fs.unlink(filePath, (error) => {
                        if (error) {
                            countFail += 1;
                        } else {
                            console.log('File deleted');
                            countSuccess += 1;
                        }

                        if (countFail + countSuccess === allFileName.length - 1) {
                            callbackFunction(err);
                        }
                    });
                }
                recursiveDeleteFile(index + 1, allFileName);
            }

            recursiveDeleteFile(index, allFileName);
        }
    });
}

module.exports = { readFile, upperCaseFile, lowerCaseFile, sortFilesContent, deleteFiles };