const { createDirectory, createFile, deleteFile } = require("../problem1.cjs");

const directoryName = 'jsonFiles'; // new directory name

createDirectory(directoryName, (err) => { // function to create new directory

    if (err) {
        console.error("Cannot create directory " + err);
    } else {

        console.log('Directory created');
        const randomNumber = Math.floor(Math.random() * 10) + 10; // Math.random() function to get random value between 10 to 20.

        createFile(directoryName, randomNumber, (err) => { // function to create random JSON files
            if (err) {
                console.error("Cannot create files " + err);
            } else {

                console.log("All files created");

                deleteFile(directoryName, randomNumber, (err) => { // function to delete random JSON files

                    if (err) {
                        console.error("Cannot delete files " + err);
                    } else {
                        console.log("All files deleted ");
                    }
                });
            }
        });
    }
});