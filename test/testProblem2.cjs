const { readFile, upperCaseFile, lowerCaseFile, sortFilesContent, deleteFiles } = require('../problem2.cjs');

readFile((err, fileContent) => { // function to read file lipsum.txt file

    if (err) {
        console.error("Cannot read directory " + err);
    } else {

        const upperCaseFileName = "upperCaseFile.txt";
        upperCaseFile(upperCaseFileName, fileContent, (err, upperCaseFileContent) => { // function to convert all the contents into uppercase and store it into new txt file

            if (err) {
                console.error("Cannot create upperCaseFile " + err);
            } else {
                const lowerCaseFileName = "lowerCaseFile.txt";
                lowerCaseFile(lowerCaseFileName, upperCaseFileContent, (err) => { // function to convert all the contents into lowercase, splitting it into new line and store it into new txt file

                    if (err) {
                        console.error("cannot create lowerCaseFile" + err);
                    } else {
                        const sortedFileName = "sortedFile.txt";
                        sortFilesContent(sortedFileName, upperCaseFileName, lowerCaseFileName, (err) => { // function to sort all the contents from the new files and store it into new txt file

                            if (err) {
                                console.error("Cannot create sortedFile" + err);
                            } else {
                                deleteFiles((err) => { // delete all the files according to the filename.txt
                                    if (err) {
                                        console.error("Cannot delete files" + err);
                                    } else {
                                        console.log("All new files deleted");
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
});